%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Package pgfplots.sty documentation. 
%
% Copyright 2007/2008 by Christian Feuersaenger.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{pgfplots.preamble.tex}
%\RequirePackage[german,english,francais]{babel}

\def\matlabcolormaptext{This colormap is similar to one shipped with Matlab (\textregistered) under a similar name.}

\IfFileExists{tikzlibraryspy.code.tex}{%
\usetikzlibrary{spy}
}{%
	\message{ERROR: tikz SPY library NOT available. The manual will only compile partially.^^J}%
}%

\usetikzlibrary{decorations.markings}

\usepgfplotslibrary{%
	ternary,
	smithchart,
	patchplots,
	polar,
	colormaps,
}
\pgfqkeys{/codeexample}{%
	every codeexample/.append style={
		/pgfplots/every ternary axis/.append style={
			/pgfplots/legend style={fill=graphicbackground},
		}
	},
	tabsize=4,
}

\pgfplotsmanualexternalexpensivetrue

%\usetikzlibrary{external}
%\tikzexternalize[prefix=figures/]{pgfplots}

\title{%
	Manual for Package \PGFPlots\\
	{\small 2D/3D Plots in \LaTeX, Version \pgfplotsversion}\\
	{\small\href{http://sourceforge.net/projects/pgfplots}{http://sourceforge.net/projects/pgfplots}}
	%\\{\small Attention: you are using an unstable development version.}
}

%\includeonly{pgfplots.libs}


\begin{document}

\def\plotcoords{%
\addplot coordinates {
(5,8.312e-02)    (17,2.547e-02)   (49,7.407e-03)
(129,2.102e-03)  (321,5.874e-04)  (769,1.623e-04)
(1793,4.442e-05) (4097,1.207e-05) (9217,3.261e-06)
};

\addplot coordinates{
(7,8.472e-02)    (31,3.044e-02)    (111,1.022e-02)
(351,3.303e-03)  (1023,1.039e-03)  (2815,3.196e-04)
(7423,9.658e-05) (18943,2.873e-05) (47103,8.437e-06)
};

\addplot coordinates{
(9,7.881e-02)     (49,3.243e-02)    (209,1.232e-02)
(769,4.454e-03)   (2561,1.551e-03)  (7937,5.236e-04)
(23297,1.723e-04) (65537,5.545e-05) (178177,1.751e-05)
};

\addplot coordinates{
(11,6.887e-02)    (71,3.177e-02)     (351,1.341e-02)
(1471,5.334e-03)  (5503,2.027e-03)   (18943,7.415e-04)
(61183,2.628e-04) (187903,9.063e-05) (553983,3.053e-05)
};

\addplot coordinates{
(13,5.755e-02)     (97,2.925e-02)     (545,1.351e-02)
(2561,5.842e-03)   (10625,2.397e-03)  (40193,9.414e-04)
(141569,3.564e-04) (471041,1.308e-04) 
(1496065,4.670e-05)
};
}%
\maketitle
\begin{abstract}%
\PGFPlots\ draws high--quality function plots in normal or logarithmic scaling with a user-friendly interface directly in \TeX. The user supplies axis labels, legend entries and the plot coordinates for one or more plots and \PGFPlots\ applies axis scaling, computes any logarithms and axis ticks and draws the plots. It supports line plots, scatter plots, piecewise constant plots, bar plots, area plots, mesh-- and surface plots, patch plots, contour plots, quiver plots, histogram plots, polar axes, ternary diagrams, smith charts and some more. It is based on Till Tantau's package \PGF/\Tikz.
\end{abstract}
\tableofcontents
\section{Introduction}
This package provides tools to generate plots and labeled axes easily. It draws normal plots, logplots and semi-logplots, in two and three dimensions. Axis ticks, labels, legends (in case of multiple plots) can be added with key-value options. It can cycle through a set of predefined line/marker/color specifications. In summary, its purpose is to simplify the generation of high-quality function and/or data plots, and solving the problems of
\begin{itemize}
	\item consistency of document and font type and font size,
	\item direct use of \TeX\ math mode in axis descriptions,
	\item consistency of data and figures (no third party tool necessary),
	\item inter-document consistency using preamble configurations and styles.
\end{itemize}
Although not necessary, separate |.pdf| or |.eps| graphics can be generated using the |external| library developed as part of \Tikz.

You are invited to use \PGFPlots\ for visualization of medium sized data sets in two and three dimensions.


\section[About PGFPlots: Preliminaries]{About {\normalfont\PGFPlots}: Preliminaries}
This section contains information about upgrades, the team, the installation (in case you need to do it manually) and troubleshooting. You may skip it completely except for the upgrade remarks.

\PGFPlots\ is built completely on \Tikz/\PGF. Knowledge of \Tikz\ will simplify the work with \PGFPlots, although it is not required.

However, note that this library requires at least \PGF\ version $2.00$. At the time of this writing, many \TeX-distributions still contain the older \PGF\ version $1.18$, so it may be necessary to install a recent \PGF\ prior to using \PGFPlots.

\subsection{Components}
\PGFPlots\ comes with two components:
\begin{enumerate}
	\item the plotting component (which you are currently reading) and
	\item the \PGFPlotstable\ component which simplifies number formatting and postprocessing of numerical tables. It comes as a separate package and has its own manual \href{file:pgfplotstable.pdf}{pgfplotstable.pdf}.
\end{enumerate}

\subsection{Upgrade remarks}
This release provides a lot of improvements which can be found in all detail in \texttt{ChangeLog} for interested readers. However, some attention is useful with respect to the following changes.

\subsubsection{New Optional Features}
\PGFPlots\ has been written with backwards compatibility in mind: old \TeX\ files should compile without modifications and without changes in the appearance. However, new features occasionally lead to a different behavior. In such a case, \PGFPlots\ will deactivate the new feature\footnote{In case of broken backwards compatibility, we apologize -- and ask you to submit a bug report. We will take care of it.}.

Any new features or bugfixes which cause backwards compatibility problems need to be activated \emph{manually} and \emph{explicitly}. In order to do so, you should use 
\begin{codeexample}[code only]
\usepackage{pgfplots}
\pgfplotsset{compat=1.5.1}
\end{codeexample}
\noindent in your preamble. This will configure the compatibility layer.

Here is a list of changes introduced in recent versions of \PGFPlots:
\begin{enumerate}
	\item \PGFPlots\ 1.5.1 interpretes circle- and ellipse radii as \PGFPlots\ coordinates (older versions used \pgfname\ unit vectors which have no direct relation to \PGFPlots). In other words: starting with version 1.5.1, it is possible to write |\draw circle[radius=5]| inside of an axis. This requires |\pgfplotsset{compat=1.5.1}| or higher. 

	Without this compatibility setting, circles and ellipses use low--level canvas units of \pgfname\ as in earlier versions.

	\item \PGFPlots\ 1.5 uses |log origin=0| as default (which influences logarithmic bar plots or stacked logarithmic plots). Older versions keep |log origin=infty|. This requires |\pgfplotsset{compat=1.5}| or higher.

	\item \PGFPlots\ 1.4 has fixed several smaller bugs which might produce differences of about $1$--$2\text{pt}$ compared to earlier releases. This requires |\pgfplotsset{compat=1.4}| or higher.

	\item \PGFPlots\ 1.3 comes with user interface improvements. The technical distinction between ``behavior options'' and ``style options'' of older versions is no longer necessary (although still fully supported).

	This is always activated.

	\item \PGFPlots\ 1.3 has a new feature which allows to \emph{move axis labels tight to tick labels} automatically. This is strongly recommended. It requires |\pgfplotsset{compat=1.3}| or higher.

	Since this affects the spacing, it is not enabled be default.

	\item \PGFPlots\ 1.3 now supports reversed axes. It is no longer necessary to use workarounds with negative units.
\pgfkeys{/pdflinks/search key prefixes in/.add={/pgfplots/,}{}}

	Take a look at the |x dir=reverse| key.

	Existing workarounds will still function properly. Use |\pgfplotsset{compat=1.3}| or higher together with |x dir=reverse| to switch to the new version.
\end{enumerate}

\subsubsection{Old Features Which May Need Attention}
\begin{enumerate}
	\item The |scatter/classes| feature produces proper legends as of version 1.3. This may change the appearance of existing legends of plots with |scatter/classes|.

	\item Starting with \PGFPlots\ $1.1$, |\tikzstyle| should \emph{no longer be used} to set \PGFPlots\ options.
	
	Although |\tikzstyle| is still supported for some older \PGFPlots\ options, you should replace any occurance of |\tikzstyle| with |\pgfplotsset{|\meta{style name}|/.style={|\meta{key-value-list}|}}| or the associated |/.append style| variant. See Section~\ref{sec:styles} for more detail.
\end{enumerate}
I apologize for any inconvenience caused by these changes.

\begin{pgfplotskey}{compat=\mchoice{1.5.1,1.5,1.4,1.3,pre 1.3,default,newest} (initially default)}
	The preamble configuration 
\begin{codeexample}[code only]
\usepackage{pgfplots}
\pgfplotsset{compat=1.5.1}
\end{codeexample}
	allows to choose between backwards compatibility and most recent features.

	Occasionally, you might want to use different versions in the same document. Then, provide
\begin{codeexample}[code only]
\begin{figure}
	\pgfplotsset{compat=1.4}
	...
	\caption{...}
\end{figure}
\end{codeexample}
	\noindent in order to restrict the compatibility setting to the actual context (in this case, the |figure| environment).

	Use |\pgfplotsset{compat=default}| to restore the factory settings.

	The setting |\pgfplotsset{compat=newest}| will always use features of the most recent version. This might result in small changes in the document's appearance. Note that it is generally a good idea to select the most recent version which is compatible with your document. This ensures that future changes will still be compatible with your document.

	Although typically unnecessary, it is also possible to activate only selected changes and keep compatibility to older versions in general:
	\begin{pgfplotskeylist}{compat/path replacement=\meta{version},compat/labels=\meta{version},compat/scaling=\meta{version},compat/general=\meta{version}}
	Let us assume that we have a document with |\pgfplotsset{compat=1.3}| and you want to keep it this way.

	In addition, you realized that version 1.5.1 supports circles and ellipses. Then, use
\begin{codeexample}[]
% preamble:
\pgfplotsset{compat=1.3,compat/path replacement=1.5.1}
\begin{tikzpicture}
\begin{axis}[
	extra x ticks={-2,2},
	extra y ticks={-2,2},
	extra tick style={grid=major}]
	\addplot {x};
	\draw (axis cs:0,0) circle[radius=2];
\end{axis}
\end{tikzpicture}
\end{codeexample}
	
	All of these keys accept the possible values of the |compat| key.

	The |compat/path replacement| key controls how radii of circles and ellipses are interpreted.

	The |compat/labels| key controls how axis labels are aligned: either uses adjacent to ticks or with an absolute offset.

	The |compat/scaling| key controls some bugfixes introduced in version 1.4: they might introduce slight scaling differences in order to improve the accuracy.

	The |compat/general| key currently only activates |log origin|.

	The detailed effects can be seen on the beginning of this section.
	\end{pgfplotskeylist}
\end{pgfplotskey}

\subsection{The Team}
\PGFPlots\ has been written mainly by Christian Feuersänger with many improvements of Pascal Wolkotte and Nick Papior Andersen as a spare time project. We hope it is useful and provides valuable plots.

If you are interested in writing something but don't know how, consider reading the auxiliary manual \href{file:TeX-programming-notes.pdf}{TeX-programming-notes.pdf} which comes with \PGFPlots. It is far from complete, but maybe it is a good starting point (at least for more literature).

\subsection{Acknowledgements}
I thank God for all hours of enjoyed programming. I thank Pascal Wolkotte and Nick Papior Andersen for their programming efforts and contributions as part of the development team. I thank Stefan Tibus, who contributed the |plot shell| feature. I thank Tom Cashman for the contribution of the |reverse legend| feature. Special thanks go to Stefan Pinnow whose tests of \PGFPlots\ lead to numerous quality improvements. Furthermore, I thank Dr.~Schweitzer for many fruitful discussions and Dr.~Meine for his ideas and suggestions. Special thanks go to Markus B\"ohning for proof-reading all the manuals of \PGF, \PGFPlots, and \PGFPlotstable. Thanks as well to the many international contributors who provided feature requests or identified bugs or simply improvements of the manual!

Last but not least, I thank Till Tantau and Mark Wibrow for their excellent graphics (and more) package \PGF\ and \Tikz, which is the base of \PGFPlots.

\include{pgfplots.install}%
\include{pgfplots.intro}%
\include{pgfplots.reference}%
\include{pgfplots.libs}%
\include{pgfplots.resources}%
\include{pgfplots.importexport}%
\include{pgfplots.basic.reference}%

\printindex

\bibliographystyle{abbrv} %gerapali} %gerabbrv} %gerunsrt.bst} %gerabbrv}% gerplain}
\nocite{pgfplotstable}
\nocite{programmingnotes}
\bibliography{pgfplots}
\end{document}
