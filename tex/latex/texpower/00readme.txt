======================================================================

			   TeXPower bundle
	   Creating dynamic online presentations with LaTeX

			  alpha version (v0.0.9d)
	   This readme file last changed on May 15, 2003

  Author: Stephan Lehmke <mailto:Stephan.Lehmke@cs.uni-dortmund.de>
	  Lehrstuhl Informatik I
	  Universitšt Dortmund
	  Dortmund, Germany

======================================================================


This bundle contains style and class files I have written for creating
dynamic online presentations with LaTeX. 

======================================================================

The TeXPower project homepage is located at SourceForge:

 http://texpower.sourceforge.net/

The mailing list is also located at SourceForge:

 http://lists.sourceforge.net/lists/listinfo/texpower-users

======================================================================

Contents:
=========

So far, the bundle contains the following files and directories:


00readme.txt

	This file.

01install.txt

	Installation instructions.

0changes.txt

	Change history.

texpower.sty

	Implements some commands for presentation effects. This
	includes 
	* setting page transitions (thanks to Marc van Dongen
	  for the code) and durations for automatic advancement of
	  pages;
	* color highlighting;
	* incremental displaying of pages;
	* navigation helpers;
	* setting page backgrounds and `panels'.

	The code should work with all ways of PDF creation.

	Thanks to Klaus Gunterman for allowing me to build this
	package on the basis of his texpause.sty from the PPower4
	bundle. 

tpcolors.cfg
tpoptions.cfg
tpsettings.cfg

	Configuration files for texpower.sty.

powersem.cls

	A wrapper for seminar which sets up everything for dynamic
	presentations. For this alpha version, it doesn't do much
	more than load seminar.cls and do some bug fixes.

	\documentclass{powersem} should be used as a replacement for
	\documentclass{seminar}. powersem loads seminar and passes all
	options to seminar.

addons (directory)

	Auxilliary packages and classes which use or augment the
	TeXPower bundle.

doc (directory)

	Contains several tex files which provide examples and
	documentation (in tex and pdf format) for TeXPower.
        Compiled examples can be downloaded from
          http://texpower.sourceforge.net/doc/

Makefile

	Doesn't do much that is useful to the end user at the
	moment. 

	make install will try to put texpower where your TeX system
	can find it.

contrib (directory)

	Contains some additions to the TeXPower bundle contributed by
	other people.



======================================================================

Disclaimer:
===========

Beware. This is work in progress. Use only if you know what you're
doing. 
During the subsequent error correction and extension of the
functionality, the syntax and implementation of the macros are liable
to change. 

So far, the files themselves contain only scarce inline documentation,
as the code is too much of a moving target to make rigorous
documentation a sensible endeavour. As soon as this bundle is ready
for beta release, I will make fully documented dtx files.

======================================================================

License:
========

The TeXPower bundle is distributed under the GNU General Public license
< http://www.gnu.org/copyleft/gpl.html >.

======================================================================

Known and open problems:
========================

The \stepwise command (which is the main part of texpower.sty so far;
see fulldemo.pdf for some examples and documentation) is
rather fragile and doesn't harmonize with some of the more
sophisticated TeX structures. The following are the main problems
known to me at the moment:

* math formatting.

  a) Everything is done several times. As \stepwise has to do a lot of
     accounting to keep track of what's going on, this can really mess
     things up.

     AMSLaTeX's measuring pass for aligned equations is taken care of
     explicitly, so I expect no problems from this side.

     \mathchoice is another matter. Currently, I'm using a kluge which
     seems to work in my own presentations, but might cause trouble
     elsewhere.
     If there's any expert who could look into the code and suggest
     something better, I'd be forever grateful.

     I don't know if there's anything else which causes things to be
     typeset several times. If there is, it has to be hacked.

  b) To leave the proper amount of blank space, \stepwise currently
     uses \phantom. This can cause problems with math spacing.

     The widths of \phantom{a+}b and a+b are different.

     As a consequence, math spacing often has to be corretced manually
     when `stepping through' formulae.

     This can be remedied by using colors instead (\hidevanish), but
     that in turn doesn't work with structured backgrounds (or is
     there a color `transparent'?).

     I'd be thankful for hints on alternatives.


* File access, labels and hyperlinks.

  As \stepwise executes everything several times, commands changing
  counters globally or accessing files are a problem. Labels and
  hyperlinks are redefined several times.

  Counters are taken care of by \stepwise. 

  Commands accessing toc files and such (like \section) are taken care
  of by the whatsit suppression mechanism (see below).

  I don't know what can be done about labels and links. They work sort
  of (giving a lot of warnings though), but I'm at a loss as what the
  `correct' behaviour is.


* duplication of specials

  Some drivers, like dvips and textures, use a color stack which is
  controlled by \special items included in the dvi file.  When page
  contents are duplicated, then these specials are also duplicated,
  which can seriously mess up the color stack.  

  texpower implements a `color stack correction' method by maintaining
  a stack of color corrections, which should counteract this effect.  

  Owing to potential performance problems, this method is turned off
  by default. If you should experience strange color switches in your
  document, turn it on with the option fixcolorstack.  

  Expect problems with color stack correction if an automatic page
  break should occur in a `colorful' document, as the drivers color
  stack and texpowers color correction stack are likely to be
  unrelated then. Remedy remaining problems by inserting explicit page
  breaks.


* catcode mongery.

  As the argument of \stepwise is read as a normal macro argument,
  constructs involving catcode changes (like \verb or language
  switches) won't work in the argument of \stepwise.

  Owing to a great suggestion by Ross Moore, I hope to remedy this at
  least to some extent until the alpha release.



======================================================================

Further development:
====================

Until the first alpha release (which should appear Real Soon Now),
I'll try to

* Fix bugs (but I need bug reports).

* Complete the documentation.

* Extend the functionality.


The following are on my agenda at the moment:

* Add further background styles (wallpaper type).

* Add rudimentary support for `fancy' steps (text flying around).

* Add rudimentary support for self-defined page transitions.

* Add rudimentary support for `pop-ups' and other things requiring
  sophisticated navigation (animations with "stop" and "rewind"
  buttons). 

* Add rudimentary support for pdf actions.

* Make a more sophisticated version of tpslifonts.sty.

* Respect catcode changes in the argument of \stepwise.

* Remove remaining quirks and problems in connection with file access,
  labels/hyperlinks and duplication of page contents.


From then until the beta release (which can go on CTAN), I'm planning
to do mainly bug fixes and write the dtx files.


After the first beta release, the following to-do list of postponed
developments has built up during recent years:

* Add `driver' files providing more sophisticated support for special
  pdf features involving forms and actions.


See the file 0changes.txt for recent changes.

