======================================================================

			   TeXPower bundle
			    Directory: addons

	    This readme file last changed on Apr 03, 2002

  Author: Stephan Lehmke <mailto:Stephan.Lehmke@cs.uni-dortmund.de>
	  Lehrstuhl Informatik I
	  Universitšt Dortmund
	  Dortmund, Germany

======================================================================


This directory contains auxiliary style and class files using and
augmenting the TeXPower bundle.


======================================================================

Contents:
=========

This directory contains the following files:


00readme.txt

	This file.


automata.sty

	Experimental package for drawing automata in the sense of
	theoretical computer science (using PSTricks) and animating
	them with TeXPower.
	Only DFA and Mealy automata are supported so far.


fixseminar.sty

	A small fix to seminar in conjunction with pdf generation
	(respect magnification in page dimensions setting).


tpslifonts.sty

	Much extended version of the `slifonts' option of
	texpower. Allows several variants of slide-specific font
	settings. 

tpsem-a4.sty

        An LaTeX2e-fied sem-a4.sty (part of seminar). 
