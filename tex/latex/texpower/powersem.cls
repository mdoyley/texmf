% powersem.cls,v 1.10 2003/05/06 12:58:59 stephanlehmke Exp
%  
% TeXPower bundle - dynamic online presentations with LaTeX
% Copyright (C) 1999-2002 Stephan Lehmke
% 
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 2
% of the License, or (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
%-----------------------------------------------------------------------------------------------------------------
% File: powersem.cls
%
% Make seminar ready for the Third Millennium ;-)
% This class loads seminar and tries to fix some problems which occur when trying to create dynamic presentations with
% the texpower package. 
%
% Some new features helpful for dynamic presentations are also added.
%
% This class is part of the TeXPower bundle, to be found at
% 
% http://texpower.sourceforge.net/
%
% The TeXPower bundle also contains a package fixseminar.sty which applies some more fixes, in particular for
% hyperref. These can not be applied here because they have to be made after hyperref is loaded.
% 
%-----------------------------------------------------------------------------------------------------------------
% Autor: Stephan Lehmke <Stephan.Lehmke@cs.uni-dortmund.de>
%
% v0.1 Oct 18, 1999 First version. So far it doesn't do much more than load seminar.
%
% v0.2 Mar 28, 2000 Force seminar to treat \paperwidth and \paperheight in a sensible manner.
%
% v0.3 May 10, 2000 Added option `calcdimensions'.
%
% v0.4 Jun 16, 2000 Now separates text from footnotes when option `display' is given.
%
% v0.5 Jun 26, 2000 \@colht is ignored by seminar, which is bad. Added a patch to \set@slidesize.
%                   calcdimensions now handles slide frames.
%
% v0.5a Jul 03, 2000 The footnote patch with option display effectively disabled setting \centerslidestrue. Fixed.
%
% v0.6  Nov 10, 2001 seminar's \output@slide kernel copied (from seminar.bg2) and modified for better color handling
%                    and to remove some overfull \vbox warnings.
%                    There was another underfull vbox warning lurking in the footnote patch. Removed.
%                    New option truepagenumbers for cooperation with texpower in case `fragmented' slide numbers (like
%                    3.5.1) are used.
%                    Make seminar's form of raggedright lines (controlled by \raggedslides) active in parboxes and
%                    minipages (and p columns in tables or arrays).
%
% v0.7  Nov 22, 2002 Inserted \nointerlineskip in page building code to heal a slight problem with vertical spacing.
%                    A small modification was neccessary wrt the KOMA option because with fixseminar v0.0.4, pdflatex
%                    slides can now be rotated.
%
% v0.8  May  6, 2003 Some fixes provided by Pascal Kockaert to (hopefully) get better behaviour with seminar's
%                    ``article'' option - many thanks!

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass%
{powersem}%
[2003/05/06 v0.8 Create online Presentations with seminar.]

%--------------------------------------------------------------------------------------------------------------

\RequirePackage{ifthen}

%--------------------------------------------------------------------------------------------------------------
% The display option is understood by all relevant classes and packages from the TeXPower bundle. It means that
% `dynamic' features are to be turned on.
% There is a boolean register display (as defined in ifthen.sty) which is set by this option and can be used to
% differentiate between slides for display and slides for printout.
% Notes are disabled automatically.

\newboolean{display}
\DeclareOption{display}{\setboolean{display}{true}\PassOptionsToClass{slidesonly}{seminar}}


%--------------------------------------------------------------------------------------------------------------
% The truepagenumbers option makes powersem count pages with the counter page, independently of the counter slide. This
% enables proper working of TeXPowers navigation buttons (some of which calculate relative page numbers) even when the
% counter slide is reset frequently (for slide numberings of the type <l>.<n>.<m>).
\newboolean{truepn@PS}
\DeclareOption{truepagenumbers}{\setboolean{truepn@PS}{true}}


%--------------------------------------------------------------------------------------------------------------
% The KOMA option tells powersem to load scrartcl (from the KOMA suite of document classes) instead of article.

\newboolean{BaseClass@PS}
\DeclareOption{KOMA}
{\setboolean{BaseClass@PS}{true}\providecommand{\baseclass}{scrartcl}}


%--------------------------------------------------------------------------------------------------------------
% The UseBaseClass option is more neutral: It tells powersem to load the class \baseclass (initially article) instead of
% article. 

\DeclareOption{UseBaseClass}
{\setboolean{BaseClass@PS}{true}\providecommand{\baseclass}{article}}


%--------------------------------------------------------------------------------------------------------------
% The reportclass option tells powersem to load the class \baseclass (initially report) instead of
% article. 

\DeclareOption{reportclass}
{\setboolean{BaseClass@PS}{true}\providecommand{\baseclass}{report}}


%--------------------------------------------------------------------------------------------------------------
% The bookclass option tells powersem to load the class \baseclass (initially report) instead of
% article. 

\DeclareOption{bookclass}
{\setboolean{BaseClass@PS}{true}\providecommand{\baseclass}{book}}


%--------------------------------------------------------------------------------------------------------------
% We need to redefine the a4paper option which is broken in seminar.cls

\DeclareOption{a4paper}{\PassOptionsToClass{a4}{seminar}}

%--------------------------------------------------------------------------------------------------------------
% The calcdimensions option tells powersem to automatically calculate slidewidth and slideheight from paper dimensions
% and margins.

\newboolean{calcdim@PS}
\DeclareOption{calcdimensions}{\setboolean{calcdim@PS}{true}}


%--------------------------------------------------------------------------------------------------------------

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{seminar}}

\ProcessOptions


%--------------------------------------------------------------------------------------------------------------
% Now we need to hack a little to make seminar load \baseclass instead of article (if one of the respectice options was
% given). 

\let\oldLC@PS=\LoadClass

\ifthenelse{\boolean{BaseClass@PS}}
{%
  \def\article@PS{article}

% Make \PassOptionsToClass and \LoadClass use \baseclass...

  \let\oldPOTC@PS=\PassOptionsToClass
  \def\PassOptionsToClass#1#2%
  {\edef\cls@PS{#2}\ifx\article@PS\cls@PS\oldPOTC@PS{#1}{\baseclass}\else\oldPOTC@PS{#1}{#2}\fi}

% Furthermore, seminar redefines \paperheight and \paperwidth, saving their value in \sem@paperheight and
% \sem@paperwidth. We undo this so the paper dimension calculation of typearea can be used.

  \renewcommand{\LoadClass}[2][]%
  {%
    \edef\cls@PS{#2}%
    \ifx\article@PS\cls@PS
      \let\paperheight\sem@paperheight\let\paperwidth\sem@paperwidth
      \oldLC@PS[#1]{\baseclass}%
      \ifarticle\else\portraittrue\fi
     \else
      \oldLC@PS[#1]{#2}%
    \fi
  }%
} 
{}


%--------------------------------------------------------------------------------------------------------------
% Finally, the seminar class and some bug fixes are loaded.

\oldLC@PS{seminar}

\AtBeginDocument
{%
\input{seminar.bug}%
\input{seminar.bg2}%
}

\expandafter\def\expandafter\set@slidesize\expandafter
{\set@slidesize\@colht\vsize}

\g@addto@macro\@arrayparboxrestore\slide@rightskip

\ifthenelse{\boolean{truepn@PS}}{\truepagenumbers}{}

\AtBeginDocument{\ifthenelse{\boolean{BaseClass@PS}}{\sliderotation{none}}{}}
  

%-----------------------------------------------------------------------------------------------------------------
% Make some changes in the seminar kernel to hopefully get better handling of text colors and avoid overfull box
% warnings when \slidetopmargin and \slidebottommargin are set too small.

\AtBeginDocument
{%
\ifarticle
\else
  \def\output@slide{%
    \begingroup
      \hoffset=-\inverseslidesmag in
      \voffset=-\inverseslidesmag in
      \setslidelength\@tempdima{%
        \ifportrait\paperwidth\else\paperheight\fi}%
      \setslidelength\@tempdimb{%
        \ifportrait\paperheight\else\paperwidth\fi}%
      \ifrotateheaders
        \@ifrotateslide{%
          \dimen@=\@tempdima
          \@tempdima=\@tempdimb
          \@tempdimb=\dimen@}%
      \fi
      % \oddsidemargin, \evensidemargin, \headheight, \footheight
      % used for scratch:
      \setslidelength\headheight\slidetopmargin
      \setslidelength\footheight\slidebottommargin
      \ifautoslidemargins
        \textwidth=\wd\@slidebox
        \oddsidemargin=\@tempdima
        \advance\oddsidemargin-\textwidth
        \divide\oddsidemargin 2
      \else
        \setslidelength\oddsidemargin\slideleftmargin
        \setslidelength\evensidemargin\sliderightmargin
        \textwidth=\@tempdima
        \advance\textwidth-\oddsidemargin
        \advance\textwidth-\evensidemargin
      \fi
      \setbox\@slidebox=\hbox to \@tempdima{{%    All double {{ around box contents added for better color handling (STL)
      %                                                         
        \kern\oddsidemargin
        \vbox to\@tempdimb{{%
          \ifnum\fancyput@flag>-1
            \hbox{\kern-\oddsidemargin\do@fancyput}%
          \fi
          \let\label\@gobble
          \let\index\@gobble
          \let\glossary\@gobble
          \vbox to\headheight{{%
            \vfill
            \color@hbox
            \normalcolor
           \slideheadfont\relax%\strut         Removed (STL)
           \hb@xt@\textwidth{\@oddhead}%
            \color@endbox
            \kern\z@}}%
          \nointerlineskip
          \vss
          \hbox to\textwidth{{\hss\box\@slidebox\hss}}%
          \vss
          \nointerlineskip
          \vbox to\footheight{{%
            \vfill
              \color@hbox
              \normalcolor
             \slidefootfont\relax%\strut         Removed (STL)
             \hb@xt@\textwidth{\@oddfoot}%
              \color@endbox
            \vfill}}%
        }}\hss}}%
      \ifrotateheaders\@ifrotateslide\rotate@slide\fi
      \insert@specials
      \shipout\box\@slidebox
    \endgroup
    \let\firstmark\botmark
    \ifthenelse{\boolean{truepn@PS}}{\stepcounter{page}}{}% <=== This will seriously upset notes! (STL)
    }%
  \@ifundefined{fancyput@flag}{\def\fancyput@flag{-1}}{}%
\fi
}

%--------------------------------------------------------------------------------------------------------------
% When producing slides to be displayed interactively, we must make absolutely sure that interactive building of slides
% doesn't move text which has already been set. So slides are not to be centered. 

\ifthenelse{\boolean{display}}
{%
  \centerslidesfalse
  \extraslideheight{0pt}%
  \renewcommand{\slideshrink}{0}%
  \def\@makeslide{%
    \ifcenterslides
      \setbox\@slidebox\vbox{{%
        \@begindvi % added 1997/04/15 SPQR
        \unvbox\@cclv
        \ifvoid\slide@footins\else
          \vskip\skip\slide@footins
          \footnoterule
          \unvbox\slide@footins
        \fi
        \vskip\z@}}
     \else
      \setbox\@slidebox\vbox to \slide@vsize{{%
        \@begindvi % added 1997/04/15 SPQR
        \unvbox\@cclv
        \ifvoid\slide@footins\vfil\else
          \vfil\vskip\skip\slide@footins
          \footnoterule
          \unvbox\slide@footins
        \fi
        \vskip\z@}}%
    \fi
    }%
  }
{}


%--------------------------------------------------------------------------------------------------------------
% If powersem is asked to calculate the slide dimensions...

\def\relax@PS{\relax}

\ifthenelse{\boolean{calcdim@PS}}
{%
  \AtBeginDocument
  {%
    \setlength{\slidewidth}{\paperwidth}%
    \advance\slidewidth by -\slideleftmargin\relax
    \advance\slidewidth by -\sliderightmargin\relax
    \ifx\theslideframe\relax@PS
     \else
      \advance\slidewidth by -2\slideframewidth\relax
      \advance\slidewidth by -2\slideframesep\relax
    \fi
    \setlength{\slideheight}{\paperheight}%
    \advance\slideheight by -\slidetopmargin\relax
    \advance\slideheight by -\slidebottommargin\relax
    \ifx\theslideframe\relax@PS
     \else
      \advance\slideheight by -2\slideframewidth\relax
      \advance\slideheight by -2\slideframesep\relax
    \fi
    }%
  }
{}

\AtBeginDocument
{%
  \@ifundefined{headwidth}{}
  {%
    \setslidelength{\headwidth}{\paperwidth}%
    \addtoslidelength{\headwidth}{-\slideleftmargin}%
    \addtoslidelength{\headwidth}{-\sliderightmargin}%
  }%
}




% Local Variables: 
% fill-column: 120
% TeX-master: t
% End: 


%%
%% End of file `powersem.cls'.
