======================================================================

			   TeXPower bundle
			  Directory: doc

	    This readme file last changed on Mar 15, 2002

  Author: Stephan Lehmke <mailto:Stephan.Lehmke@cs.uni-dortmund.de>
	  Lehrstuhl Informatik I
	  Universitšt Dortmund
	  Dortmund, Germany

======================================================================

This directory contains several tex files which provide examples and
documentation (in tex and pdf format) for TeXPower. Compiled examples 
can be downloaded from
          http://texpower.sourceforge.net/doc/

======================================================================
