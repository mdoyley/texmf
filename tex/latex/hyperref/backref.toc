\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Usage}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Options}{1}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Options for type of back references}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Language options}{2}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Other options}{2}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Detecting the end of a bibliography entry}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Formatting of the back references list}{3}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Formatting hook \cs {backref}}{3}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Formatting hook \cs {backrefalt}}{3}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Refinement \cs {backrefentrycount}}{4}{subsubsection.2.3.3}
\contentsline {section}{\numberline {3}The macros}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Package identification}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Options}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Option verbose}{4}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Options for way of working}{5}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Language options}{5}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Process options}{7}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}The bibliography}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Reading .brf file}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Initialization}{11}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Collecting back cite informations}{12}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Rerun warning}{13}{subsection.3.7}
