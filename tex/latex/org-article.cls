
% Identification part
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{org-article}[2010/09/21 0.2 (TSD)]
% End of the identification part
%
% Initial code part

\RequirePackage{ifthen}
\RequirePackage{calc}
\RequirePackage{ifpdf}

% End of initial code part
% Declaration of options part
% Org-mode default packages
\newboolean{nofontenc}  
\DeclareOption{nofontenc}{\setboolean{nofontenc}{true}}
\newboolean{nofixltx2e}
\DeclareOption{nofixltx2e}{\setboolean{nofixltx2e}{true}}
\newboolean{nographicx}
\DeclareOption{nographicx}{\setboolean{nographicx}{true}}
\newboolean{nolongtable}
\DeclareOption{nolongtable}{\setboolean{nolongtable}{true}}
\newboolean{nofloat}
\DeclareOption{nofloat}{\setboolean{nofloat}{true}}
\newboolean{nowrapfig}
\DeclareOption{nowrapfig}{\setboolean{nowrapfig}{true}}
\newboolean{nosoul}
\DeclareOption{nosoul}{\setboolean{nosoul}{true}}
\newboolean{notextcomp}
\DeclareOption{notextcomp}{\setboolean{notextcomp}{true}}
\newboolean{nomarvosym}
\DeclareOption{nomarvosym}{\setboolean{nomarvosym}{true}}
\newboolean{nowasysym}
\DeclareOption{nowasysym}{\setboolean{nowasysym}{true}}
\newboolean{integrals}
\DeclareOption{integrals}{\setboolean{integrals}{true}}
\newboolean{nointegrals}
\DeclareOption{nointegrals}{\setboolean{nointegrals}{true}}
\newboolean{nolatexsym}
\DeclareOption{nolatexsym}{\setboolean{nolatexsym}{true}}
\newboolean{noamssymb}
\DeclareOption{noamssymb}{\setboolean{noamssymb}{true}}
\newboolean{nohyperref}
\DeclareOption{nohyperref}{\setboolean{nohyperref}{true}}

% Font options
\newboolean{times}
\DeclareOption{times}{\setboolean{times}{true}}
\newboolean{garamond}
\DeclareOption{garamond}{\setboolean{garamond}{true}}
\newboolean{palatino}
\DeclareOption{palatino}{\setboolean{palatino}{true}}
\newboolean{utopia}
\DeclareOption{utopia}{\setboolean{utopia}{true}}
\newboolean{charter}
\DeclareOption{charter}{\setboolean{charter}{true}}

% Base class options
\newboolean{koma}
\DeclareOption{koma}{\setboolean{koma}{true}}
\newboolean{article}
\DeclareOption{article}{\setboolean{article}{true}}
\newboolean{tocdepths}
\newboolean{tocdepthss}
\newboolean{tocdepthsss}  
\DeclareOption{tocdepths}{\AtBeginDocument{\setcounter{tocdepth}{1}}}
\DeclareOption{tocdepthss}{\AtBeginDocument{\setcounter{tocdepth}{2}}}
\DeclareOption{tocdepthsss}{\AtBeginDocument{\setcounter{tocdepth}{3}}}
\newboolean{secnums}
\newboolean{secnumss}
\newboolean{secnumsss}  
\newboolean{secnump}
\newboolean{secnumsp}
\DeclareOption{secnums}{\AtBeginDocument{\setcounter{secnumdepth}{1}}}
\DeclareOption{secnumss}{\AtBeginDocument{\setcounter{secnumdepth}{2}}}
\DeclareOption{secnumsss}{\AtBeginDocument{\setcounter{secnumdepth}{3}}}
\DeclareOption{secnump}{\AtBeginDocument{\setcounter{secnumdepth}{4}}}
\DeclareOption{secnumsp}{\AtBeginDocument{\setcounter{secnumdepth}{5}}}

% Other package options
\newboolean{microtype}
\DeclareOption{microtype}{\setboolean{microtype}{true}}
\newboolean{paralist}
\DeclareOption{paralist}{\setboolean{paralist}{true}}
\newboolean{setspace}
\newboolean{doublespace}
\DeclareOption{setspace}{\setboolean{setspace}{true}}
\DeclareOption{doublespace}{\setboolean{doublespace}{true}}
% \newboolean{topcapt}
% \DeclareOption{topcapt}{\setboolean{topcapt}{true}}
\newboolean{listings}
\newboolean{color}
\DeclareOption{listings}{\setboolean{listings}{true}}
\DeclareOption{listings-bw}{%
  \setboolean{listings}{true}%
  \AtBeginDocument{%
    \lstset{
      basicstyle=\ttfamily\footnotesize,%
      frame=lines,%
      breaklines=true,%
      showstringspaces=false}%
  }%
}
\DeclareOption{listings-color}{%
  \setboolean{listings}{true}%
  \setboolean{color}{true}%
  \AtBeginDocument{%
    \definecolor{keywords}{RGB}{255,0,90}%
    \definecolor{comments}{RGB}{60,179,113}%
    \definecolor{back}{RGB}{231,231,231}%
    \lstset{%
      keywordstyle=\color{keywords},%
      commentstyle=\color{comments},%
      backgroundcolor=\color{back},%
      basicstyle=\ttfamily\footnotesize,%
      showstringspaces=false,%
      frame=lines,%
      breaklines=true,%
      resetmargins=true%
    }%
  }%
}
\DeclareOption{listings-sv}{%
  \setboolean{listings}{true}%
  \setboolean{color}{true}%
  \AtBeginDocument{%
    \definecolor{...@lstbackground}{RGB}{255,255,204} % light yellow
    \definecolor{...@lstkeyword}{RGB}{0,0,255} % blue
    \definecolor{...@lstidentifier}{RGB}{0,0,0} % black
    \definecolor{...@lstcomment}{RGB}{255,0,0} % red
    \definecolor{...@lststring}{RGB}{0,128,0} % dark green
    \lstset{%
      basicstyle=\ttfamily\scriptsize, % the font that is used for the code
      tabsize=4, % sets default tabsize to 4 spaces
      numbers=left, % where to put the line numbers
      numberstyle=\tiny, % line number font size
      stepnumber=0, % step between two line numbers
      breaklines=true, %!! do break long lines of code
      showtabs=false, % show tabs within strings adding particular underscores
      showspaces=false, % show spaces adding particular underscores
      showstringspaces=false, % underline spaces within strings
      keywordstyle=\color{...@lstkeyword},
      identifierstyle=\color{...@lstidentifier},
      stringstyle=\color{...@lststring},
      commentstyle=\color{...@lstcomment},
      backgroundcolor=\color{...@lstbackground}, % sets the background color
      resetmargins=true,%
      captionpos=b, % sets the caption position to `bottom'
      extendedchars=false %!?? workaround for when the listed file is in UTF-8
    }%
  }%
}
\DeclareOption{listings-es}{%
  \setboolean{listings}{true}%
  \setboolean{color}{true}%
  \AtBeginDocument{%
    \definecolor{dkgreen}{rgb}{0,0.5,0}%
    \definecolor{dkred}{rgb}{0.5,0,0}%
    \definecolor{gray}{rgb}{0.5,0.5,0.5}%
    \lstset{%
      basicstyle=\ttfamily\bfseries\scriptsize,
      keywordstyle=\color{blue},
      ndkeywordstyle=\color{red},
      commentstyle=\color{dkred},
      stringstyle=\color{dkgreen},
      numbers=left,
      breaklines=true,
      numberstyle=\ttfamily\footnotesize\color{gray},
      stepnumber=1,
      numbersep=10pt,
      backgroundcolor=\color{white},
      tabsize=4,
      showspaces=false,
      showstringspaces=false,
      xleftmargin=.23in
    }%
  }%
}  
\newboolean{minted}
\DeclareOption{minted}{\setboolean{minted}{true}}  
%  \newboolean{color}
%  \DeclareOption{color}{\setboolean{color}{true}}
%

% Base class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}  

% Pass options to packages
\DeclareOption{anchorcolor}{%
   \PassOptionsToPackage{anchorcolor}{hyperref}}
\DeclareOption{backref}{%
   \PassOptionsToPackage{backref}{hyperref}}
\DeclareOption{baseurl}{%
   \PassOptionsToPackage{baseurl}{hyperref}}
\DeclareOption{bookmarks}{%
   \PassOptionsToPackage{bookmarks}{hyperref}}
\DeclareOption{bookmarksnumbered}{%
   \PassOptionsToPackage{bookmarksnumbered}{hyperref}}
\DeclareOption{bookmarksopen}{%
   \PassOptionsToPackage{bookmarksopen}{hyperref}}
\DeclareOption{bookmarksopenlevel}{%
   \PassOptionsToPackage{bookmarksopenlevel}{hyperref}}
\DeclareOption{bookmarkstype}{%
   \PassOptionsToPackage{bookmarkstype}{hyperref}}
\DeclareOption{breaklinks}{%
   \PassOptionsToPackage{breaklinks}{hyperref}}
\DeclareOption{CJKbookmarks}{%
   \PassOptionsToPackage{CJKbookmarks}{hyperref}}
\DeclareOption{citebordercolor}{%
   \PassOptionsToPackage{citebordercolor}{hyperref}}
\DeclareOption{citecolor}{%
   \PassOptionsToPackage{citecolor}{hyperref}}
\DeclareOption{colorlinks}{%
   \PassOptionsToPackage{colorlinks}{hyperref}}
\DeclareOption{draft}{%
   \PassOptionsToPackage{draft}{hyperref}}
\DeclareOption{dvipdfm}{%
   \PassOptionsToPackage{dvipdfm}{hyperref}}
\DeclareOption{dvipdfmx}{%
   \PassOptionsToPackage{dvipdfmx}{hyperref}}
\DeclareOption{dvips}{%
   \PassOptionsToPackage{dvips}{hyperref}}
\DeclareOption{dvipsone}{%
   \PassOptionsToPackage{dvipsone}{hyperref}}
\DeclareOption{dviwindo}{%
   \PassOptionsToPackage{dviwindo}{hyperref}}
\DeclareOption{encap}{%
   \PassOptionsToPackage{encap}{hyperref}}
\DeclareOption{extension}{%
   \PassOptionsToPackage{extension}{hyperref}}
\DeclareOption{filebordercolor}{%
   \PassOptionsToPackage{filebordercolor}{hyperref}}
\DeclareOption{filecolor}{%
   \PassOptionsToPackage{filecolor}{hyperref}}
\DeclareOption{final}{%
   \PassOptionsToPackage{final}{hyperref}}
\DeclareOption{frenchlinks}{%
   \PassOptionsToPackage{frenchlinks}{hyperref}}
\DeclareOption{hyperfigures}{%
   \PassOptionsToPackage{hyperfigures}{hyperref}}
\DeclareOption{hyperfootnotes}{%
   \PassOptionsToPackage{hyperfootnotes}{hyperref}}
\DeclareOption{hyperindex}{%
   \PassOptionsToPackage{hyperindex}{hyperref}}
\DeclareOption{hypertex}{%
   \PassOptionsToPackage{hypertex}{hyperref}}
\DeclareOption{hypertexnames}{%
   \PassOptionsToPackage{hypertexnames}{hyperref}}
\DeclareOption{implicit}{%
   \PassOptionsToPackage{implicit}{hyperref}}
\DeclareOption{latex2html}{%
   \PassOptionsToPackage{latex2html}{hyperref}}
\DeclareOption{legalpaper}{%
   \PassOptionsToPackage{legalpaper}{hyperref}}
\DeclareOption{letterpaper}{%
   \PassOptionsToPackage{letterpaper}{hyperref}}
\DeclareOption{linkbordercolor}{%
   \PassOptionsToPackage{linkbordercolor}{hyperref}}
\DeclareOption{linkcolor}{%
   \PassOptionsToPackage{linkcolor}{hyperref}}
\DeclareOption{linktocpage}{%
   \PassOptionsToPackage{linktocpage}{hyperref}}
\DeclareOption{menubordercolor}{%
   \PassOptionsToPackage{menubordercolor}{hyperref}}
\DeclareOption{menucolor}{%
   \PassOptionsToPackage{menucolor}{hyperref}}
\DeclareOption{nativepdf}{%
   \PassOptionsToPackage{nativepdf}{hyperref}}
\DeclareOption{naturalnames}{%
   \PassOptionsToPackage{naturalnames}{hyperref}}
\DeclareOption{nesting}{%
   \PassOptionsToPackage{nesting}{hyperref}}
\DeclareOption{pageanchor}{%
   \PassOptionsToPackage{pageanchor}{hyperref}}
\DeclareOption{pagebackref}{%
   \PassOptionsToPackage{pagebackref}{hyperref}}
\DeclareOption{pdfauthor}{%
   \PassOptionsToPackage{pdfauthor}{hyperref}}
\DeclareOption{pdfborder}{%
   \PassOptionsToPackage{pdfborder}{hyperref}}
\DeclareOption{pdfcenterwindow}{%
   \PassOptionsToPackage{pdfcenterwindow}{hyperref}}
\DeclareOption{pdfcreator}{%
   \PassOptionsToPackage{pdfcreator}{hyperref}}
\DeclareOption{pdfdirection}{%
   \PassOptionsToPackage{pdfdirection}{hyperref}}
\DeclareOption{pdfdisplaydoctitle}{%
   \PassOptionsToPackage{pdfdisplaydoctitle}{hyperref}}
\DeclareOption{pdfduplex}{%
   \PassOptionsToPackage{pdfduplex}{hyperref}}
\DeclareOption{pdffitwindow}{%
   \PassOptionsToPackage{pdffitwindow}{hyperref}}
\DeclareOption{pdfhighlight}{%
   \PassOptionsToPackage{pdfhighlight}{hyperref}}
\DeclareOption{pdfinfo}{%
   \PassOptionsToPackage{pdfinfo}{hyperref}}
\DeclareOption{pdfkeywords}{%
   \PassOptionsToPackage{pdfkeywords}{hyperref}}
\DeclareOption{pdflang}{%
   \PassOptionsToPackage{pdflang}{hyperref}}
\DeclareOption{pdfmark}{%
   \PassOptionsToPackage{pdfmark}{hyperref}}
\DeclareOption{pdfmenubar}{%
   \PassOptionsToPackage{pdfmenubar}{hyperref}}
\DeclareOption{pdfnewwindow}{%
   \PassOptionsToPackage{pdfnewwindow}{hyperref}}
\DeclareOption{pdfnonfullscreenpagemode}{%
   \PassOptionsToPackage{pdfnonfullscreenpagemode}{hyperref}}
\DeclareOption{pdfnumcopies}{%
   \PassOptionsToPackage{pdfnumcopies}{hyperref}}
\DeclareOption{pdfpagelayout}{%
   \PassOptionsToPackage{pdfpagelayout}{hyperref}}
\DeclareOption{pdfpagemode}{%
   \PassOptionsToPackage{pdfpagemode}{hyperref}}
\DeclareOption{pdfpagelabels}{%
   \PassOptionsToPackage{pdfpagelabels}{hyperref}}
\DeclareOption{pdfpagescrop}{%
   \PassOptionsToPackage{pdfpagescrop}{hyperref}}
\DeclareOption{pdfpagetransition}{%
   \PassOptionsToPackage{pdfpagetransition}{hyperref}}
\DeclareOption{pdfpicktraybypdfsize}{%
   \PassOptionsToPackage{pdfpicktraybypdfsize}{hyperref}}
\DeclareOption{pdfprintarea}{%
   \PassOptionsToPackage{pdfprintarea}{hyperref}}
\DeclareOption{pdfprintclip}{%
   \PassOptionsToPackage{pdfprintclip}{hyperref}}
\DeclareOption{pdfprintpagerange}{%
   \PassOptionsToPackage{pdfprintpagerange}{hyperref}}
\DeclareOption{pdfprintscaling}{%
   \PassOptionsToPackage{pdfprintscaling}{hyperref}}
\DeclareOption{pdfproducer}{%
   \PassOptionsToPackage{pdfproducer}{hyperref}}
\DeclareOption{pdfstartpage}{%
   \PassOptionsToPackage{pdfstartview}{hyperref}}
\DeclareOption{pdfsubject}{%
   \PassOptionsToPackage{pdfsubject}{hyperref}}
\DeclareOption{pdftex}{%
   \PassOptionsToPackage{pdftex}{hyperref}}
\DeclareOption{pdftitle}{%
   \PassOptionsToPackage{pdftitle}{hyperref}}
\DeclareOption{pdftoolbar}{%
   \PassOptionsToPackage{pdftoolbar}{hyperref}}
\DeclareOption{pdftrapped}{%
   \PassOptionsToPackage{pdftrapped}{hyperref}}
\DeclareOption{pdfview}{%
   \PassOptionsToPackage{pdfview}{hyperref}}
\DeclareOption{pdfviewarea}{%
   \PassOptionsToPackage{pdfviewarea}{hyperref}}
\DeclareOption{pdfviewclip}{%
   \PassOptionsToPackage{pdfviewclip}{hyperref}}
\DeclareOption{pdfwindowui}{%
   \PassOptionsToPackage{pdfwindowui}{hyperref}}
\DeclareOption{plainpages}{%
   \PassOptionsToPackage{plainpages}{hyperref}}
\DeclareOption{ps2pdf}{%
   \PassOptionsToPackage{ps2pdf}{hyperref}}
\DeclareOption{raiselinks}{%
   \PassOptionsToPackage{raiselinks}{hyperref}}
\DeclareOption{runbordercolor}{%
   \PassOptionsToPackage{runbordercolor}{hyperref}}
\DeclareOption{runcolor}{%
   \PassOptionsToPackage{runcolor}{hyperref}}
\DeclareOption{setpagesize}{%
   \PassOptionsToPackage{setpagesize}{hyperref}}
\DeclareOption{tex4ht}{%
   \PassOptionsToPackage{tex4ht}{hyperref}}
\DeclareOption{textures}{%
   \PassOptionsToPackage{textures}{hyperref}}
\DeclareOption{unicode}{%
   \PassOptionsToPackage{unicode}{hyperref}}
\DeclareOption{urlbordercolor}{%
   \PassOptionsToPackage{urlbordercolor}{hyperref}}
\DeclareOption{urlcolor}{%
   \PassOptionsToPackage{urlcolor}{hyperref}}
\DeclareOption{verbose}{%
   \PassOptionsToPackage{verbose}{hyperref}}
\DeclareOption{vtex}{%
   \PassOptionsToPackage{vtex}{hyperref}}
\DeclareOption{xetex}{%
   \PassOptionsToPackage{xetex}{hyperref}}

% End of declaration of options part
% Execution of options part

\ProcessOptions\relax


% End of execution of options part
% Package loading part
  
  % Base class
  \ifthenelse{\boolean{koma}}
  {%
    \LoadClass{scrartcl}%
  }%
  {%
  \LoadClass{article}%
  }
  
  % Org-mode default
  \ifthenelse{\boolean{nofixltx2e}}
  {}
  {\RequirePackage{fixltx2e}}    
  \ifthenelse{\boolean{nographicx}}
  {}
  {\RequirePackage{graphicx}}   
  \ifthenelse{\boolean{nolongtable}}
  {}
  {\RequirePackage{longtable}}    
  \ifthenelse{\boolean{nofloat}}
  {}
  {\RequirePackage{float}}  
  \ifthenelse{\boolean{nowrapfig}}
  {}
  {\RequirePackage{wrapfig}}  
  \ifthenelse{\boolean{nosoul}}
  {}
  {\RequirePackage{soul}}  
  \ifthenelse{\boolean{nofontenc}}
  {}
  {\RequirePackage[T1]{fontenc}}    % with T1 option for symbol packages
  \ifthenelse{\boolean{notextcomp}}
  {}
  {\RequirePackage{textcomp}}  
  \ifthenelse{\boolean{nomarvosym}}
  {}
  {\RequirePackage{marvosym}}  
  \ifthenelse{\boolean{nowasysym}}
  {}
  {%
    \ifthenelse{\boolean{integrals}}%
    {\RequirePackage[integrals]{wasysym}}%
    {\RequirePackage[nointegrals]{wasysym}}%
  }  
  \ifthenelse{\boolean{nolatexsym}}
  {}
  {\RequirePackage{latexsym}}  
  \ifthenelse{\boolean{noamssymb}\or\boolean{utopia}\or\boolean{charter}\or\boolean{garamond}\or\boolean{times}}
  {}
  {\RequirePackage{amssymb}}  
  
  % Other packages
  % Set the standard LaTeX list environments to their compact counterparts  
  \ifthenelse{\boolean{paralist}}
    {%
      \RequirePackage{paralist}
      \let\itemize\compactitem%
      \let\description\compactdesc%
      \let\enumerate\compactenum%
    }
    {}  
  \ifthenelse{\boolean{microtype}}
  {%
    \ifpdf
     \RequirePackage{microtype}
    \fi}%
  {}
  \ifthenelse{\boolean{setspace}}
  {\RequirePackage{setspace}}
  {}
  % \ifthenelse{\boolean{topcapt}}
  %   {\RequirePackage{topcapt}}
  %   {}
  \ifthenelse{\boolean{listings}}
    {\RequirePackage{listings}}
    {}
  \ifthenelse{\boolean{color}}
    {\RequirePackage{color}}
    {}
  \ifthenelse{\boolean{minted}}
    {\RequirePackage{minted}}
    {}
%  
   
  % Font packages 
  \ifthenelse{\boolean{times}}
  {%
    \ifpdf
    \RequirePackage[T1]{fontenc}
    \RequirePackage{mathptmx} 
    \RequirePackage[scaled=.90]{helvet} 
    \RequirePackage{courier}
    \fi}%
  {}
  \ifthenelse{\boolean{garamond}}
  {%
    \ifpdf
     \RequirePackage[T1]{fontenc} 
     \RequirePackage[urw-garamond]{mathdesign}
     \RequirePackage[scaled]{berasans} 
     \RequirePackage{inconsolata} % tt
     \linespread{1.0609}
    \fi}%
  {}
  \ifthenelse{\boolean{palatino}}
  {%
    \ifpdf
    \RequirePackage[T1]{fontenc}
    \RequirePackage{mathpazo}% 
    \linespread{1.05}%
    \RequirePackage[scaled]{helvet}%
    \RequirePackage{courier} % tt
    \fi}%
  {}  
  \ifthenelse{\boolean{charter}}
  {%
    \ifpdf
     \RequirePackage[T1]{fontenc} 
     \RequirePackage[bitstream-charter]{mathdesign}
     \RequirePackage[scaled=.90]{helvet} 
     \RequirePackage{courier} % tt
    \fi}%
  {}  
  \ifthenelse{\boolean{utopia}}
  {%
    \ifpdf
     \RequirePackage[T1]{fontenc} 
     \RequirePackage[adobe-utopia]{mathdesign}
     \RequirePackage[scaled]{berasans} 
     \RequirePackage{inconsolata} % tt
    \fi}%
  {}  

  % Hyperref asks to be loaded last
  \ifthenelse{\boolean{nohyperref}}
  {}
  {\RequirePackage{hyperref}}  
  
  % End of package loading part
  %
% Class code part
 \ifthenelse{\boolean{setspace}}%
 {\ifthenelse{\boolean{doublespace}}%
 {\doublespacing}%
 {\singlespacing}}%
 {}%
 \ifthenelse{\boolean{listings}}%
 {\lstdefinelanguage{org}%
   {%
     morekeywords={:results, :session, :var, :noweb, :exports},%
     sensitive=false,%
     morestring=[b]",%
     morecomment=[l]{\#},%
   }%
   \lstdefinelanguage{dot}
   {%
     morekeywords={graph},
     sensitive=false,
   }%
   \lstdefinelanguage{ditaa}
   {%
     breaklines=false
   }%
 }%
 {}%
% 
 % End of class code part
