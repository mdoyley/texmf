\contentsline {section}{\numberline {1}List of Figures}{2}{section.1}
\contentsline {section}{\numberline {2}List of Tables}{2}{section.2}
\contentsline {section}{\numberline {3}Introduction}{2}{section.3}
\contentsline {section}{\numberline {4}Data Types}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Conditionals}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}\textsf {ifthen} conditionals}{11}{subsection.4.2}
\contentsline {section}{\numberline {5}Fixed Point Arithmetic}{15}{section.5}
\contentsline {section}{\numberline {6}Strings}{21}{section.6}
\contentsline {section}{\numberline {7}Databases}{23}{section.7}
\contentsline {subsection}{\numberline {7.1}Creating a New Database}{23}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Loading a Database from an External ASCII File}{25}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Displaying the Contents of a Database}{28}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Iterating Through a Database}{32}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}Null Values}{42}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}Editing Database Rows}{45}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}Arithmetical Computations on Database Entries}{46}{subsection.7.7}
\contentsline {subsection}{\numberline {7.8}Sorting a Database}{50}{subsection.7.8}
\contentsline {subsection}{\numberline {7.9}Saving a Database to an External File}{53}{subsection.7.9}
\contentsline {subsection}{\numberline {7.10}Deleting or Clearing a Database}{54}{subsection.7.10}
\contentsline {subsection}{\numberline {7.11}Advanced Database Commands}{54}{subsection.7.11}
\contentsline {section}{\numberline {8}Pie Charts (\textsf {datapie} package)}{56}{section.8}
\contentsline {subsection}{\numberline {8.1}Pie Chart Variables}{61}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Pie Chart Label Formatting}{62}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Pie Chart Colours}{62}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Adding Extra Commands Before and After the Pie Chart}{64}{subsection.8.4}
\contentsline {section}{\numberline {9}Scatter and Line Plots (\textsf {dataplot} package)}{65}{section.9}
\contentsline {subsection}{\numberline {9.1}Adding Information to the Plot}{70}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Global Plot Settings}{72}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}Lengths}{72}{subsubsection.9.2.1}
\contentsline {subsubsection}{\numberline {9.2.2}Counters}{73}{subsubsection.9.2.2}
\contentsline {subsubsection}{\numberline {9.2.3}Macros}{73}{subsubsection.9.2.3}
\contentsline {subsection}{\numberline {9.3}Adding to a Plot Stream}{74}{subsection.9.3}
\contentsline {section}{\numberline {10}Bar Charts (\textsf {databar} package)}{77}{section.10}
\contentsline {subsection}{\numberline {10.1}Changing the Appearance of a Bar Chart}{78}{subsection.10.1}
\contentsline {section}{\numberline {11}Converting a {\rmfamily B\kern -.05em\textsc {i\kern -.025em b}\kern -.08emT\kern -.1667em\lower .7ex\hbox {E}\kern -.125emX} database into a \textsf {datatool} database (\textsf {databib} package)}{85}{section.11}
\contentsline {subsection}{\numberline {11.1}{\rmfamily B\kern -.05em\textsc {i\kern -.025em b}\kern -.08emT\kern -.1667em\lower .7ex\hbox {E}\kern -.125emX}: An Overview}{86}{subsection.11.1}
\contentsline {subsubsection}{\numberline {11.1.1}{\rmfamily B\kern -.05em\textsc {i\kern -.025em b}\kern -.08emT\kern -.1667em\lower .7ex\hbox {E}\kern -.125emX} database}{86}{subsubsection.11.1.1}
\contentsline {subsection}{\numberline {11.2}Loading a \textsf {databib} database}{88}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Displaying a \textsf {databib} database}{89}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Changing the bibliography style}{92}{subsection.11.4}
\contentsline {subsubsection}{\numberline {11.4.1}Modifying an existing style}{92}{subsubsection.11.4.1}
\contentsline {subsection}{\numberline {11.5}Iterating through a \textsf {databib} database}{95}{subsection.11.5}
\contentsline {subsection}{\numberline {11.6}Multiple Bibliographies}{97}{subsection.11.6}
\contentsline {section}{\numberline {12}Referencing People (\textsf {person} package)}{99}{section.12}
\contentsline {subsection}{\numberline {12.1}Defining and Undefining People}{99}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Displaying Information}{100}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Advanced Commands}{104}{subsection.12.3}
\contentsline {subsubsection}{\numberline {12.3.1}Conditionals}{104}{subsubsection.12.3.1}
\contentsline {subsubsection}{\numberline {12.3.2}Iterating Through Defined People}{105}{subsubsection.12.3.2}
\contentsline {subsubsection}{\numberline {12.3.3}Accessing Individual Information}{105}{subsubsection.12.3.3}
\contentsline {section}{\numberline {13}datatool.sty}{106}{section.13}
\contentsline {subsection}{\numberline {13.1}Package Declaration}{106}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Package Options}{106}{subsection.13.2}
\contentsline {subsection}{\numberline {13.3}Determining Data Types}{113}{subsection.13.3}
\contentsline {subsection}{\numberline {13.4}ifthen Conditionals}{134}{subsection.13.4}
\contentsline {subsection}{\numberline {13.5}Defining New Databases}{138}{subsection.13.5}
\contentsline {subsection}{\numberline {13.6}Accessing Data}{150}{subsection.13.6}
\contentsline {subsection}{\numberline {13.7}Iterating Through Databases}{156}{subsection.13.7}
\contentsline {subsection}{\numberline {13.8}Displaying Database}{175}{subsection.13.8}
\contentsline {subsection}{\numberline {13.9}Editing Databases}{180}{subsection.13.9}
\contentsline {subsection}{\numberline {13.10}Database Functions}{183}{subsection.13.10}
\contentsline {subsection}{\numberline {13.11}Sorting Databases}{193}{subsection.13.11}
\contentsline {subsection}{\numberline {13.12}Saving a database to an external file}{200}{subsection.13.12}
\contentsline {subsection}{\numberline {13.13}Loading a database from an external file}{202}{subsection.13.13}
\contentsline {subsection}{\numberline {13.14}General List Utilities}{209}{subsection.13.14}
\contentsline {subsection}{\numberline {13.15}General Token Utilities}{212}{subsection.13.15}
\contentsline {subsection}{\numberline {13.16}Floating Point Arithmetic}{212}{subsection.13.16}
\contentsline {subsection}{\numberline {13.17}String Macros}{220}{subsection.13.17}
\contentsline {subsection}{\numberline {13.18}Currencies}{226}{subsection.13.18}
\contentsline {subsection}{\numberline {13.19}Debugging commands}{227}{subsection.13.19}
\contentsline {section}{\numberline {14}datapie.sty}{228}{section.14}
\contentsline {section}{\numberline {15}dataplot.sty}{236}{section.15}
\contentsline {section}{\numberline {16}databar.sty}{254}{section.16}
\contentsline {section}{\numberline {17}databib.sty}{271}{section.17}
\contentsline {subsection}{\numberline {17.1}Package Declaration}{271}{subsection.17.1}
\contentsline {subsection}{\numberline {17.2}Package Options}{271}{subsection.17.2}
\contentsline {subsection}{\numberline {17.3}Loading BBL file}{271}{subsection.17.3}
\contentsline {subsection}{\numberline {17.4}Predefined text}{272}{subsection.17.4}
\contentsline {subsection}{\numberline {17.5}Displaying the bibliography}{272}{subsection.17.5}
\contentsline {subsubsection}{\numberline {17.5.1}ifthen conditionals}{283}{subsubsection.17.5.1}
\contentsline {subsection}{\numberline {17.6}Bibliography Style Macros}{286}{subsection.17.6}
\contentsline {subsection}{\numberline {17.7}Bibliography Styles}{289}{subsection.17.7}
\contentsline {subsection}{\numberline {17.8}Multiple Bibliographies}{304}{subsection.17.8}
\contentsline {section}{\numberline {18}person.sty}{306}{section.18}
\contentsline {subsection}{\numberline {18.1}Package Declaration}{306}{subsection.18.1}
\contentsline {subsection}{\numberline {18.2}Defining People}{306}{subsection.18.2}
\contentsline {subsection}{\numberline {18.3}Remove People}{307}{subsection.18.3}
\contentsline {subsection}{\numberline {18.4}Conditionals and Loops}{308}{subsection.18.4}
\contentsline {subsection}{\numberline {18.5}Predefined Words}{310}{subsection.18.5}
\contentsline {subsection}{\numberline {18.6}Displaying Information}{312}{subsection.18.6}
\contentsline {subsection}{\numberline {18.7}Extracting Information}{319}{subsection.18.7}
\contentsline {section}{References}{320}{section*.3}
\contentsline {section}{\numberline {19}References}{320}{section.19}
\contentsline {section}{Acknowledgements}{320}{DTLbibrow.103.bib.1}
\contentsline {section}{Change History}{320}{section*.5}
