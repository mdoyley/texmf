\contentsline {section}{\numberline {1}Introduction}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Historical notes}{4}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}The classes of SI units}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}The SI prefixes}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Acronyms}{5}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Some useful definitions}{5}{subsection.1.5}
\contentsline {section}{\numberline {2}SI units}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}SI base units}{6}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Definitions}{6}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Symbols}{8}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}SI derived units}{8}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Units expressed in terms of base units}{8}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}SI derived units with special names and symbols}{8}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Use of SI derived units with special names and symbols}{10}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Dimension of a quantity}{10}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Units for dimensionless quantities, quantities of dimension one}{11}{subsubsection.2.3.1}
\contentsline {subsection}{\numberline {2.4}Rules and style conventions for writing and using SI unit symbols}{11}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Space between numerical value and unit symbol}{12}{subsubsection.2.4.1}
\contentsline {section}{\numberline {3}SI Prefixes}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Decimal multiples and submultiples of SI units}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Rules for using SI prefixes}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}The kilogram}{13}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}The `degree Celsius'}{14}{subsubsection.3.2.2}
\contentsline {section}{\numberline {4}Prefixes for binary multiples}{14}{section.4}
\contentsline {subsection}{\numberline {4.1}Official publication}{14}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}The \texttt {binary.sty} style for binary prefixes and (non-SI) units}{15}{subsection.4.2}
\contentsline {section}{\numberline {5}Units outside the SI}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Units accepted for use with the SI}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Units temporarily accepted for use with the SI}{15}{subsection.5.2}
\contentsline {section}{\numberline {6}Last notes about correct usage of the SI}{15}{section.6}
\contentsline {section}{\numberline {7}How to use the package}{17}{section.7}
\contentsline {subsection}{\numberline {7.1}Loading}{17}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}The package options}{18}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Unit spacing options}{18}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Quantity-unit spacing options}{18}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Options to prevent conflicts}{18}{subsubsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.4}textstyle}{19}{subsubsection.7.2.4}
\contentsline {subsubsection}{\numberline {7.2.5}miscellaneous}{19}{subsubsection.7.2.5}
\contentsline {subsection}{\numberline {7.3}How to compose units in your text.}{20}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Division or multiplication of SI units}{20}{subsubsection.7.3.1}
\contentsline {paragraph}{Division}{20}{section*.18}
\contentsline {paragraph}{Multiplication}{20}{section*.19}
\contentsline {paragraph}{Mixed case}{20}{section*.20}
\contentsline {subsubsection}{\numberline {7.3.2}Raising SI units to a power}{20}{subsubsection.7.3.2}
\contentsline {paragraph}{Squaring and cubing}{20}{section*.21}
\contentsline {paragraph}{The reciprocal, reciprocal squaring and - cubing}{22}{section*.22}
\contentsline {paragraph}{The power function}{22}{section*.23}
\contentsline {subsection}{\numberline {7.4}Quantities and units}{22}{subsection.7.4}
\contentsline {subsubsection}{\numberline {7.4.1}Ready-to-use units}{22}{subsubsection.7.4.1}
\contentsline {section}{\numberline {8}How the package works}{24}{section.8}
\contentsline {subsection}{\numberline {8.1}Compatibility}{24}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Known problems and limitations}{24}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Sending a bug report}{25}{subsection.8.3}
\contentsline {section}{\numberline {9}In conclusion}{25}{section.9}
\contentsline {subsection}{\numberline {9.1}Acknowledgements}{25}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}References}{25}{subsection.9.2}
