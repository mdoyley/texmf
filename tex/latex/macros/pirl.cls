\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[2012/1/07 v0.1]
\LoadClass[12pt,letterpaper]{article}
\RequirePackage[sc]{mathpazo}
\RequirePackage[margin=1in]{geometry} % set page borders
\RequirePackage{color} % set page borders
\RequirePackage{graphicx}
\RequirePackage{calc}
\RequirePackage{tikz}     % tikz drawing package
\RequirePackage{pgfplots} %
\RequirePackage{fixltx2e}
\RequirePackage{setspace}
\RequirePackage[english]{babel}
\RequirePackage{wrapfig}
\RequirePackage{soul}
\RequirePackage[inline]{trackchanges}
\RequirePackage{hyperref}
\RequirePackage{latexsym}
\RequirePackage{marvosym}
\RequirePackage{fancyhdr}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{wasysym}
\RequirePackage{latexsym}
\RequirePackage{amssymb}
\RequirePackage{subfig} 

\pgfplotsset{compat=1.5.1}
\doublespacing
% my commands
\newcommand{\insrt}[1]{\textcolor{blue}{#1}}
\newcommand{\commen}[1]{{\textcolor{red}{#1}}}
\newcommand{\vect}[1]{\boldsymbol{#1}}
\newcommand{\Image}[3]{\begin{figure}\begin{center}\includegraphics[scale=#1]{#2}\caption{#3}\end{center}\end{figure}}        
\newcommand{\mdeg}{\ensuremath{^\circ}}
\renewcommand{\Re}{\text{Re }}
\renewcommand{\Im}{\text{Im }}
\newcommand*{\trace}[1]{\ensuremath{Tr\protect\left(#1\protect\right)}}% trace of matrix
\newcommand*{\transpose}[1]{\ensuremath{#1\sp{t}}}
\newcommand*{\inv}[1]{\ensuremath{#1\sp{-1}}}
\newcommand*{\adj}[1]{\ensuremath{#1\sp{\ast}}}
\newcommand{\ft}{\mathcal{F}}
\newcommand{\ift}{\mathcal{F}^{-1}}


\endinput
