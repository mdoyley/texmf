(TeX-add-style-hook
 "header_beamer"
 (lambda ()
   (TeX-add-symbols
    '("eqn" 1)
    '("cntr" 1)
    '("Image" 2)
    '("sqb" 1)
    '("clyb" 1)
    '("cvb" 1)
    '("padd" 1)
    '("summation" 2)
    '("integrate" 2)
    '("grad" 1)
    '("curl" 1)
    '("uv" 1)
    '("abs" 1)
    '("var" 1)
    '("ave" 1)
    '("pdd" 2)
    "degree"
    "sinc"))
 :latex)

