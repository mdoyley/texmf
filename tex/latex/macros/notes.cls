\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[2012/1/07 v0.1]
\LoadClass[12pt,letterpaper]{article}
%\RequirePackage[sc]{mathpazo}
\RequirePackage[margin=1in]{geometry} % set page borders
\RequirePackage{color} % set page borders
\RequirePackage{graphicx}
\RequirePackage{calc}
\RequirePackage{hyperref}
\RequirePackage{tikz}     % tikz drawing package
\RequirePackage{pgfplots} %
\RequirePackage{fixltx2e}
\RequirePackage{setspace}
\RequirePackage[english]{babel}
\RequirePackage{wrapfig}
\RequirePackage{soul}
\RequirePackage[inline]{trackchanges}
\RequirePackage{hyperref}
\RequirePackage{latexsym}
\RequirePackage{marvosym}
\RequirePackage{fancyhdr}
\RequirePackage[utf8]{inputenc}
%\RequirePackage[graphics,tightpage,active]{preview}
%\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}
\RequirePackage{wasysym}
\RequirePackage{amssymb}
\RequirePackage{subfig} 
\pgfplotsset{compat=1.5.1}
\doublespacing
\usetikzlibrary{calc}
\usetikzlibrary{arrows}
%\PreviewEnvironment{tikzpicture}
%% my commands
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\Image}[3]{\begin{figure}\begin{center}\includegraphics[scale=#1]{#2}\caption{#3}\end{center}\end{figure}}        
\newcommand{\mdeg}{\ensuremath{^\circ}}
\renewcommand{\Re}{\text{Re }}
\renewcommand{\Im}{\text{Im }}
\newcommand*{\trace}[1]{\ensuremath{Tr\protect\left(#1\protect\right)}}% trace of matrix
\newcommand*{\transpose}[1]{\ensuremath{#1\sp{t}}}
\newcommand*{\inv}[1]{\ensuremath{#1\sp{-1}}}
\newcommand*{\adj}[1]{\ensuremath{#1\sp{\ast}}}
\newcommand{\ft}{\mathcal{F}}
\newcommand{\ift}{\mathcal{F}^{-1}}
\newcommand{\pdd}[2]{\frac{\partial {#1}}{\partial {#2}}}     % partial derivative
\newcommand{\dotp}[2]{{#1}\cdot{#2}}                              % dot product
\newcommand{\sint}[2]{\ensuremath{\int_{#1}^{#2}}}              %Intregral with lower and 
\newcommand{\dint}[1]{\ensuremath{\iint\limits_{#1}}}           %double Intregral with lower and 
\newcommand{\tint}[1]{\ensuremath{\iiint\limits_{#1}}}          %tripple Intregral 
\newcommand{\cint}[1]{\ensuremath{\oint\limits_{#1}}}          %tripple Intregral  
\newcommand{\summa}[2]{\displaystyle{\sum_{#1}^{#2}}}     
\newcommand{\C}{\mathrm{\cos}}
\newcommand{\w}{\omega}
\newcommand{\Dx}{\mathcal{D}x}
\renewcommand{\d}[2]{\frac{d #1}{d #2}} % for derivatives
\newcommand{\dd}[2]{\frac{d^2 #1}{d #2^2}} % for double derivatives


\def\micrns{\mu{m}}							                  % microns
\newcommand{\ave}[1]{\left\langle#1\right\rangle} 				  % average <x> 
\newcommand{\var}[1]{\sigma}                                      % for variance
\newcommand{\uv}[1]{\ensuremath{\mathbf{\hat{#1}}}}               % for unit vector 
\def\del{\bigtriangledown}                                        % del                      
\renewcommand{\div}[1]{\gv{\nabla} \cdot #1}                      % for divergence
\newcommand{\curl}[1]{\gv{\nabla} \times #1}                      % for curl
\newcommand{\grad}[1]{\gv{\nabla} #1}                             % for gradient   
\newcommand{\integrate}[2]{\ensuremath{\int_{#1}^{#2}}}           %Intregral with lower and upper limits  
\newcommand{\summation}[2]{\displaystyle{\sum_{#1}^{#2}}}     
\newcommand{\cvb}[1]{\left(#1\right)} % ( ) delimiters
\newcommand{\clyb}[1]{\left\{#1\right\}} % { } delimiters
\newcommand{\sqb}[1]{\left[#1\right]} % [ ] delimiters 
\newcommand{\sinc}{{\rm sinc}}        %
\newcommand{\cntr}[1]{\begin{center}#1\end{center}}
\newcommand{\eqn}[1]{\begin{equation}#1\end{equation}}

\newcommand{\norm}[2][-1]{
  \ifthenelse{\equal{#1}{-1}}{ % automatic sizing
	\left\lVert #2 \right\rVert}{
  \ifthenelse{\equal{#1}{0}}{ % compact sizing
	\lVert #2 \rVert}{
	\text{INVALID OPTION FOR \verb|\norm|}}}
}
% Absolute value
\newcommand{\abs}[2][-1]{
  \ifthenelse{\equal{#1}{-1}}{ % automatic sizing
	\left\lvert {#2} \right\rvert}{
  \ifthenelse{\equal{#1}{0}}{ % compact sizing
	\lvert {#2} \rvert}{
	\text{INVALID OPTION FOR \verb|\abs|}}}

}
% Average or expectation value
\newcommand{\avg}[2][-1]{
  \ifthenelse{\equal{#1}{-1}}{ % automatic sizing
	\left< {#2} \right>}{
  \ifthenelse{\equal{#1}{0}}{ % compact sizing
	\langle {#2} \rangle}{
	\text{INVALID OPTION FOR \verb|\abs|}}}
}

% Make some adjustments to maketitle and
\makeatletter % Need for anything that contains an @ command
\newcommand{\subtitle}[1]{\def\@subtitle{#1}}
\subtitle{}
% Clearing variables
\global\let\@title\@empty
\global\let\@subtitle\@empty
\global\let\@author\@empty

\renewcommand{\maketitle}{ % Redefine maketitle to conserve space, include subtitle, and center properly in all cases
  \begingroup
  \begin{center}
     \ifx\@title\@empty
	\relax
     \else
	\Large \textbf{\@title} \\
     \fi
     \ifx\@subtitle\@empty
	\relax
     \else
	\vspace{3pt} \Large \@subtitle \\
     \fi
     \ifx\@author\@empty
	\ifx\@date\@empty
	   \relax
	   \vspace{10pt}
	\else
	   \vspace{7pt} \large \@date \\
	   \vspace{10pt}
	\fi
     \else
	\ifx\@date\@empty
	   \vspace{7pt} \large \@author \\
	   \vspace{10pt}
	\else
	   \vspace{7pt} \large \@author \hspace{20pt} \@date \\
	   \vspace{10pt}
	\fi
     \fi
  \end{center}
  \endgroup
  \vspace{-25pt}
 \setcounter{footnote}{0}
}
\makeatother % End of region containing @ commands

\endinput
